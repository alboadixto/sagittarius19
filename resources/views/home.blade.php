@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Bienvenido</h1>
@stop

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="card card-primary">

                    <div class="card-header">
                        <h3 class="card-title">Consulta</h3>
                    </div>

                    <div class="card-body">
                        <form action="{{route('home')}}" method="GET">
                            <div class="row">
                                <div class="col-md-5">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            <i class="far fa-user"></i>
                                            </span>
                                        </div>
                                        <select class="form-control" name="nombre" required>
                                            <option value="">Seleccione usuario</option>

                                            @foreach ($datas as $value)
                                                <option value="{{ $value->nombre }}" {{ ($value->nombre == $selectedID) ? 'selected' : '' }}>
                                                    {{ $value->nombre }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                </div>

                                <div class="col-md-5">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control float-right" name="daterange_filter" id="daterange_filter">
                                    </div>
                                </div>

                                </div>

                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-block bg-gradient-primary">Buscar</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

                @if(!$distances)
                    <div class="col-12 alert alert-info alert-dismissible">
                        <h5><i class="icon fas fa-info"></i> Sin registros!</h5>
                        Los filtros seleccionados no contienen registros.
                    </div>
                @else
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title"><span id="card-title">Datos</span></h3>
                        </div>

                        <!-- /.card-header -->
                        <div class="card-body">
                            <dl class="row">
                                @foreach ($distances as $distance)
                                <dt class="col-sm-4 text-center">{{$distance->usuario}}</dt>

                                    {{-- fecha --}}
                                    <dd class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p class="text-muted">Fecha:</p>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$distance->fecha}}
                                            </div>
                                        </div>
                                    </dd>

                                    <dt class="col-sm-4"></dt>
                                    {{-- distancia --}}
                                    <dd class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p class="text-muted">Distancia:</p>
                                            </div>
                                            <div class="col-sm-10">
                                                {{$distance->distance}} mts
                                            </div>
                                        </div>
                                    </dd>

                                    <dt class="col-sm-4"></dt>
                                    {{-- cantidad --}}
                                    <dd class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <p class="text-muted">Tiempo:</p>
                                            </div>
                                            <div class="col-sm-10">
                                                @php
                                                    $horas = floor($distance->cantidad / 3600);
                                                    $minutos = floor(($distance->cantidad - ($horas * 3600)) / 60);
                                                    $segundos = $distance->cantidad - ($horas * 3600) - ($minutos * 60);
                                                @endphp
                                                {{$segundos}} segundo(s)
                                            </div>
                                        </div>
                                    </dd>
                                @endforeach
                            </dl>
                        </div>
                        <!-- /.card-body -->
                    </div>
                @endif

            </div>
        </div>
    </div>
@stop

@section('js')
    <script>

        $(function () {

            const fecha_ini   = "{{$fecha_ini}}";
            const fecha_fin   = "{{$fecha_fin}}";

            //Date range picker
            $('#daterange_filter').daterangepicker({
                "startDate" : fecha_ini,
                "endDate"   : fecha_fin,
                "locale"    : {
                    "format"            : "YYYY-MM-DD",
                    "separator"         : " | ",
                    "applyLabel"        : "Aplicar",
                    "cancelLabel"       : "Cancelar",
                    "fromLabel"         : "DE",
                    "toLabel"           : "HASTA",
                    "customRangeLabel"  : "Custom",
                    "daysOfWeek"        : [ "Dom",
                                            "Lun",
                                            "Mar",
                                            "Mie",
                                            "Jue",
                                            "Vie",
                                            "Sáb"],
                    "monthNames"        : [ "Enero",
                                            "Febrero",
                                            "Marzo",
                                            "Abril",
                                            "Mayo",
                                            "Junio",
                                            "Julio",
                                            "Agosto",
                                            "Septiembre",
                                            "Octubre",
                                            "Noviembre",
                                            "Diciembre"],
                    "firstDay"          : 1
                }
            })

        })

    </script>
@stop
