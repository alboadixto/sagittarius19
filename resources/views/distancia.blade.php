@if($distancia < 3)
    <span class="text-sm badge bg-danger">{{$distancia}}</span>
@elseif($distancia == 3)
    <span class="text-sm badge bg-warning">{{$distancia}}</span>
@else
    <span class="text-sm badge bg-success">{{$distancia}}</span>
@endif
