<?php

namespace App\Traits;

use App\Models\Data;
use App\Models\Distance;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

trait DistanceTrait
{
    public function distance(){
        $names = Data::where('nombre', '<>', '')
            ->where('status', 0)
            ->groupBy('nombre')
            ->select('nombre')
            ->get();

        if(count($names) > 1){
            foreach($names as $name){
                $user_primary = Data::select(
                    'nombre',
                    'created_at',
                    'cord_latid',
                    'cord_longi')
                    ->where('nombre', $name->nombre)
                    ->get();

                foreach($user_primary as $primary){
                    $addDateTime        = Carbon::parse($primary->created_at)->addMinutes(1); //2021-01-22 20:42:23

                    $user_secondary = Data::select(
                        'nombre',
                        'created_at',
                        'cord_latid',
                        'cord_longi')
                        ->where('nombre', '<>', $primary->nombre)
                        ->whereBetween('created_at', [$primary->created_at, $addDateTime])
                        ->get();

                    foreach($user_secondary as $secondary){
                        // VARIABLES
                        $point1 = array(
                            "lat" => $primary->cord_latid,
                            "long" => "$primary->cord_longi");
                        $point2 = array(
                            "lat" => $secondary->cord_latid,
                            "long" => $secondary->cord_longi);

                        //* CALCULO DISTANCIA
                        $distanceInMts      = $this->distanceCalculation(
                            $point1['lat'],
                            $point1['long'],
                            $point2['lat'],
                            $point2['long'],
                            'mts');

                        if($distanceInMts < 10){
                            //* INSERTAR LAS DISTANCIAS DE LOS USUARIOS
                            Distance::create([
                                'user_primary'      => $primary->nombre,
                                'user_secondary'    => $secondary->nombre,
                                'date'              => $secondary->created_at,
                                'distance'          => $distanceInMts
                            ]);
                        }

                    }

                }
            }

            //* ACTUALIZAR LOS REGISTROS QUE YA SE PROCESARON
            DB::table('data')->where('status', 0)->update(['status' => 1]);
        }
    }

    private function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {

        // Cálculo de la distancia en grados
        $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));

        // Conversión de la distancia en grados a la unidad escogida (kilómetros o metros)
        switch($unit) {
            case 'km':
                $distance = $degrees * 111.13384; // 1 grado = 111.13384 km, basándose en el diametro promedio de la Tierra (12.735 km)
                break;
            case 'mts':
                $distance =  ($degrees * 111.13384) * 1000;
        }

        return round($distance, $decimals);
    }

}
