<?php

namespace App\Http\Controllers;

use App\Models\Data;
use Illuminate\Http\Request;
use App\Traits\DistanceTrait;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    use DistanceTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $nombre      = $request->nombre ? $request->nombre : '';
        $distances   = [];
        $fecha_ini   = date('Y-m-d')." 00:00:00";
        $fecha_fin   = date('Y-m-d')." 23:59:59";

        //* DATERANGE
        if($request->daterange_filter){
            $fecha_rango    = explode(" | ", $request->daterange_filter);
            $fecha_ini      = $fecha_rango[0]." 00:00:00";
            $fecha_fin      = $fecha_rango[1]." 23:59:59";
        }

        //* SELECT NOMBRE
        $datas      = Data::where('nombre', '<>', '')->groupBy('nombre')->select('nombre')->get();
        $selectedID = $nombre;

        //* INFORMACIÓN DISTANCIA
        if($nombre){
            $this->distance();

            /* $distances   = Distance::where(function($query) use ($nombre) {
                    return $query->where('user_primary', $nombre)
                        ->orWhere('user_secondary', $nombre);
                })
                ->whereBetween('date', [$fecha_ini, $fecha_fin])
                ->get();
            */

            $distances  = DB::select("
                select usuario,
                fecha,
                min(distance) as distance,
                count(*) as cantidad
                from (
                    select
                    IF(user_primary = '$nombre', user_secondary, user_primary) as usuario,
                    CAST(date AS DATE) AS fecha,
                    distance
                    from distances
                    where (user_primary = '$nombre' or user_secondary = '$nombre')
                    and CAST(date AS DATE) between '$fecha_ini' and '$fecha_fin'
                    order by fecha, usuario
                ) t
                group by usuario, fecha");
        }

        return view('home', compact('selectedID', 'datas', 'nombre', 'fecha_ini', 'fecha_fin', 'distances'));
    }
}
