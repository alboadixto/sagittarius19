<?php

namespace App\Http\Controllers;

use App\Models\Data;
use Illuminate\Http\Request;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //* CREAR DATA
        $data = Data::create([
            'uuid'              => $request->uuid,
            'cord_latid'        => $request->latitude,
            'cord_longi'        => $request->longitude,
            'accuracy'          => $request->accuracy,
            'speed'             => $request->speed,
            'odometer'          => $request->odometer,
            'event'             => $request->event,
            'is_moving'         => $request->is_moving,
            'activity'          => $request->activity_type,
            'batery'            => $request->battery_level,
            'nombre'            => $request->nombre,
            'distancia'         => $request->distancia,
            'tiempo_exposicion' => $request->tiempo_exposicion
        ]);

        $res = array(
            'status'    => 'Created',
            'code'      => 201,
            'message'   => 'El registro se ha creado correctamente.',
            'data'      => $data
        );

        return response()->json($res, $res['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
